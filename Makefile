install:
	pip install -r requirements.txt
	python manage.py migrate


start:
	python manage.py runserver 8000


publish:
	@python ./cli.py publish