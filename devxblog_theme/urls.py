from django.conf.urls.static import static
from django.urls import path

urlpatterns = [
                  path('', ThemePostList.as_view(), name='post_list'),
                  path('post', ThemePostView.as_view(), name='post_view'),
                  path('page', ThemePageView.as_view(), name='page_view'),
              ] + static('/static', document_root='devxblog_theme/static')
